package Series;

public class SpyNumbers {
    public static void main(String[] args) {
        int a=1124;
        int sum=0;
        int mul=1;
        while(a!=0){
            int r=a%10;
            sum=sum+r;//1+1+2+4=8
            mul=mul*r;//1*1*2*4=8
            a=a/10;
        }
        if(sum==mul)
            System.out.println("Number is a SpyNumber");
    }
}
