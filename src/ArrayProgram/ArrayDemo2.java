package ArrayProgram;

public class ArrayDemo2 {
    public static void main(String[] args) {
        int arr[]={1,2,3,4,5};
        //enhanced for loop
        for (int a:arr){
            System.out.println(a);
        }
    }
}
