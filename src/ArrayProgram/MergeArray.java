package ArrayProgram;

import java.util.Arrays;

public class MergeArray {
    public static void main(String[] args) {
        int[] arr1={1,2,3,4};
        int[] arr2={5,6,7,8,9};
        int[] arr3=new int[arr1.length+arr2.length];
        int ind=0;
        for (int a:arr1){
            arr3[ind]=a;
            ind++;
        }

        for (int a:arr2){
            arr3[ind]=a;
            ind++;
        }
        System.out.println(Arrays.toString(arr3));

        for (int a:arr3){
            System.out.println(a);
        }
    }
}
