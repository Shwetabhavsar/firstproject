package ArrayProgram;

import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.Scanner;

public class ArrayDemo1 {
    public static void main(String[] args) {

        Scanner sc1 = new Scanner(System.in);
        boolean status = true;
        while (status) {
            System.out.println("Select product");
            System.out.println("1.TV\n 2.PROJECTOR\n 3.MOBILE\n 4.EXIT ");
            int choice = sc1.nextInt();
            StoreManager s1 = new StoreManager();
            if (choice == 1 || choice == 2 || choice == 3) {
                System.out.println("Enter Quantity");
                int qty = sc1.nextInt();
                s1.calculateBill(choice, qty);
            } else if (choice == 4) {
                break;
            } else {
                System.out.println("invalid choice");
                status = false;
            }
        }
        }
    }
    class StoreManager {

        String[] product = {"TV", "PROJECTOR", "MOBILE"};
        double[] cost = {12000.25, 15000, 20000};
        int[] stock = {50, 30, 20};

        void calculateBill(int choice, int qty) {
            boolean found = false;
            for (int a = 0; a < product.length; a++) {
                if (choice == a + 1 && qty <= stock[a]) {
                    double total = qty * cost[a];
                    stock[a] -= qty;
                    System.out.println("Total bill amount:" + total);
                    found = true;
                }
            }
            if (!found) {
                System.out.println("product not found or out of stock");
            }
        }
    }


