package PatternPrograms;

public class PatternDemo28 {
    public static void main(String[] args) {
        int lines=5;
        int space=4;
        int star=1;
        int ch=1;

        for(int i=0;i<lines;i++){
            for (int k=0;k<space;k++) {
                System.out.print(" ");
            }
            for (int j=0;j<star;j++){
                if(i==j)
                    System.out.print(ch);
                else
                    System.out.print("*");
            }
            System.out.println();
                ch++;
                space--;
                star+=2;

        }
    }
}
