package PatternPrograms;

public class Program1 {
    public static void main(String[] args) {
        int rows = 5;
        int star=5;
        for (int i=0;i<rows;i++){
            for (int j=0;j<star;j++){
                System.out.print("*\t");
            }
            System.out.println();
        }
    }
}
