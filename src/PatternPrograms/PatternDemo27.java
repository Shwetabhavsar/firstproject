package PatternPrograms;

public class PatternDemo27 {
    public static void main(String[] args) {
        int lines=9;
        int space=4;
        int star=1;

        for (int i=0;i<lines;i++){
            char ch='A';
            for (int k=0;k<space;k++) {
                System.out.print("    ");
            }
            for (int j=0;j<star;j++){
                System.out.print(ch++ +"\t\t");

            }
            System.out.println();
            if(i<=3) {
                space--;
                star++;
            }
            else {
                space++;
                star--;
            }
        }
    }

}
