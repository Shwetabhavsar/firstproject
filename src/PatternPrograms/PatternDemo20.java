package PatternPrograms;

public class PatternDemo20 {
    public static void main(String[] args) {
        int lines=5;
        int star=4;
        int ch1;
        for(int i=0;i<lines;i++){
            ch1=1;
            //int alphabet=65;
            char alphabet='A';
            for(int j=0;j<star;j++){
                if (i%2==0){
                    System.out.print(alphabet+"\t");
                    alphabet++;
                }
                else
                    System.out.print(ch1++  +"\t");
            }
            System.out.println();
        }
    }
}
