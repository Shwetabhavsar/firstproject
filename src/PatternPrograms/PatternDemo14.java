package PatternPrograms;

public class PatternDemo14 {
    public static void main(String[] args) {
        int lines=5;
        int no=5;
        for (int i=0;i<lines;i++){
            int ch=1;
            for (int j=0;j<no;j++){
                System.out.print(ch+"\t");
                ch++;
            }
            System.out.println();
        }
    }
}

