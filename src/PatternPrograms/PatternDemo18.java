package PatternPrograms;

public class PatternDemo18 {
    public static void main(String[] args) {
        int lines=5;
        int star=4;
        int ch=1;
        for (int i=0;i<lines;i++){

            for (int j=0;j<star;j++){
                if (i%2==0)//check i for even num, if even print 'char' else '*'
                    System.out.print(ch+"\t");
                else
                    System.out.print("*\t");

            }
            System.out.println();
            ch++;

        }
    }
}
