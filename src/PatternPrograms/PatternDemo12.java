package PatternPrograms;

public class PatternDemo12 {
    public static void main(String[] args) {
        int lines=5;
        int star=5;
        for (int i=0;i<lines;i++){
            for(int j=0;j<i+1;j++){
                System.out.print("*\t");
            }
            System.out.println();
        }
    }
}
