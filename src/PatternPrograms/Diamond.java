package PatternPrograms;

public class Diamond {
        public static void main(String[] args) {
            int lines=5;
            int space=2;
            int star=1;

            for(int i=0;i<lines;i++){
                for (int k=0;k<space;k++) {
                    System.out.print(" \t");
                }
                for (int j=0;j<star;j++){
                    System.out.print("*\t");
                }
                System.out.println();
                if(i<=1) {
                    space--;
                    star+=2;
                }
                else {

                    space++;
                    star -= 2;
                }

            }
        }
    }


