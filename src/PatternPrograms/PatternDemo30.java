package PatternPrograms;

public class PatternDemo30 {
        public static void main(String[] args) {
            int lines = 7;
            int space = 3;
            int star = 1;

            for (int i = 0; i < lines; i++) {

                for (int j = 0; j < star; j++) {
                    if (j==star-1 ||j==star-2)
                        System.out.print("*");
                    else
                        System.out.print(" ");
                }
                System.out.println();
                if(i<=2){
                    star++;
                    space--;
                }
                else {
                    star--;
                    space++;
                }
            }
        }
    }



