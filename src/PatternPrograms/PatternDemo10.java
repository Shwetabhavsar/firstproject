package PatternPrograms;

public class PatternDemo10 {
    public static void main(String[] args) {
        int rows =5;
        int star=5;
        for (int i=0;i<rows;i++){
            for (int j=0;j<star;j++){
                if(i==j) {
                    System.out.print("*\t");
                }
                else if(i==2||j==2){
                    System.out.print(" \t");
                }
                else{
                    System.out.print("*\t");
                }
            }
            System.out.println();
        }
    }
    }

