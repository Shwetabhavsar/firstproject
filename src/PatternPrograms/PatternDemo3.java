package PatternPrograms;

public class PatternDemo3 {
    public static void main(String[] args) {
        int rows = 5;
        int star = 5;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < star; j++) {
                if (i==j||i == 0 || i == 4 || j == 0 || j == 4) {
                    System.out.print("*\t");
                } else {
                    System.out.print(" \t");
                }

            }
            System.out.println();
        }
    }
}


