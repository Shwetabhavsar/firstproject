package PatternPrograms;

public class HollowDiamond {
        public static void main(String[] args) {
            int lines=5;
            int space=2;
            int star=1;

            for(int i=0;i<lines;i++){
                for (int k=0;k<space;k++) {
                    System.out.print(" ");
                }
                for (int j=0;j<star;j++){
                    if(j==0 || j==star-1)
                        System.out.print("*");
                    else
                        System.out.print(" ");
                }
                System.out.println();
                if(i<=1) {
                    space--;
                    star+=2;
                }
                else {
                    space++;
                    star -= 2;
                }
            }
        }
    }




