package PatternPrograms;

public class PatternDemo13 {
    public static void main(String[] args) {
        int lines=5;
        int no=5;
        int ch=1;
        for (int i=0;i<lines;i++){
            for (int j=0;j<no;j++){
                System.out.print(ch+"\t");
            }
            System.out.println();
            ch++;
        }
    }
}

