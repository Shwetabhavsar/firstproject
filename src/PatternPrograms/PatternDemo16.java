package PatternPrograms;

public class PatternDemo16 {
    public static void main(String[] args) {
        int lines=5;
        int no=5;
        int ch1=1;
        for (int i=0;i<lines;i++){
            int ch2=1;
            for (int j=0;j<no;j++){
                System.out.print(ch2 * ch1 +"\t");
                ch2++;
            }
            System.out.println();
            ch1++;
        }
    }
}
