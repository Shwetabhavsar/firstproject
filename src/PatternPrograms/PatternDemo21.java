package PatternPrograms;

public class PatternDemo21 {
    public static void main(String[] args) {
        int lines=5;
        int star=5;
        int ch=1;
        for(int i=0;i<lines;i++){
             ch=1;
            for (int j=0;j<star;j++){
                if (j%2==0) {
                    System.out.print(ch + "\t");
                    ch++;
                }
                else {
                    System.out.print("*\t");
                }
            }
            System.out.println();
            }
        }
    }


