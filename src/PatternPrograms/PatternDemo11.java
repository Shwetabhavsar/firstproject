package PatternPrograms;

public class PatternDemo11 {
    public static void main(String[] args) {
        int lines=5;
        int star=5;
        for (int i=lines;i>=1;i--){
            for(int j=1;j<=i;j++){
                System.out.print("*\t");
            }
            System.out.println();
        }
    }
}
