package PatternPrograms;

public class PatternDemo25 {
    public static void main(String[] args) {
        int lines=5;
        int space=lines-1;
        int star=1;

        int num1=5;
        for(int i=0;i<lines;i++){
            int num2=num1;
            for (int k=0;k<space;k++) {
                System.out.print(" \t");
            }
            for (int j=0;j<star;j++){
                System.out.print(num2+"\t");
                num2++;
            }
            System.out.println();
            space--;
            star++;
            num1--;

        }
    }
}
