package PatternPrograms;

public class PatternDemo17 {
    public static void main(String[] args) {
        int lines=4;
        int star=5;
        int ch=1;
        for(int i=0;i<lines;i++){
            for(int j=0;j<star;j++){
                System.out.print(ch++ +"\t");
                if(ch>7) //if(ch>'E') { ch='A'}
                    ch=1;
            }
            System.out.println();
        }
    }
}
