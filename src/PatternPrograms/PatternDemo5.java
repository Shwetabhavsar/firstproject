package PatternPrograms;

public class PatternDemo5 {
    public static void main(String[] args) {
        int rows=5;
        int star =5;
        for (int i=0;i<rows;i++){
            for(int j=0;j<star;j++){
                if(i+j==4||i==0||j==0)
                    System.out.print(" * ");
                else
                    System.out.print("   ");
            }
            System.out.println();
        }
    }

}

