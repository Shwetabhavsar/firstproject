package PatternPrograms;

public class PatternDemo19 {
    public static void main(String[] args) {
        int lines=5;
        int star=4;
        int ch=1;
        for(int i=0;i<lines;i++){
            for (int j=0;j<star;j++){
                if (i%2==0) {
                    System.out.print(ch + "\t");
                }
                else {
                    System.out.print("*\t");
                }
            }
            System.out.println();
            if(i%2==0){
                ch++;
            }
        }
    }
}
